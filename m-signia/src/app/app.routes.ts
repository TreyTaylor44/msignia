import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './modules/auth/auth.component';

export const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
  },
  { path: '**', redirectTo: 'login' },
];

export const AppRouter: ModuleWithProviders<any> = RouterModule.forRoot(routes);
