import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _apiService: BaseApiService
  ) {}

  login(data) {
    return this._apiService.post('/api/login', data);
  }

  twoFa(data) {
    return this._apiService.post('/api/otp', data);
  }
}
