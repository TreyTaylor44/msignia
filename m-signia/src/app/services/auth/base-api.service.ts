import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {
  private readonly _baseUrl: string;
  private readonly _domainUrl: string;

  private _options: any;
  private _headers = new HttpHeaders();

  constructor(
    private _httpClient: HttpClient
  ) {
    this._baseUrl = environment.serverUrlApi;
    this._domainUrl = environment.domainUrl;
    this._headers = this._headers.append('Content-Type', 'application/json').append('Access-Control-Allow-Origin', this._domainUrl);

    this._options = {
      headers: this._headers
    };
  }

  get(url: string, params?: HttpParams): Observable<any> {
    return this._httpClient.get(this._baseUrl + url, {...this._options, params});
  }

  post(url: string, body: string): Observable<any> {
    return this._httpClient.post(this._baseUrl + url, body, this._options);
  }

  delete(url: string) {
    return this._httpClient.delete(this._baseUrl + url, this._options);
  }
}
