import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from '../components/login/login.component';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from "../components/spinner/spinner.component";
import { CountdownModule } from "ngx-countdown";

const COMPONENTS = [
  LoginComponent,
  SpinnerComponent
];

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  RouterModule,
  CountdownModule
];

@NgModule({
  imports: [
    ...MODULES
  ],
  exports: [
    ...COMPONENTS,
    ...MODULES
  ],
  declarations: [
    ...COMPONENTS
  ]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders<any> {
    return <ModuleWithProviders<any>>{
      ngModule: SharedModule,
      providers: []
    };
  }
}
