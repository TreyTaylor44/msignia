import {Component, OnInit, ViewChild} from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { NgForm } from "@angular/forms";
import { CountdownComponent, CountdownConfig } from "ngx-countdown";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('cd', { static: false }) private _countdown: CountdownComponent;

  public userLogin = {
    email: '',
    password: '',
    secret: ''
  };
  public error: any;
  public success: boolean;
  public secondStep: boolean;
  public spinner: boolean;
  public expired: boolean;
  public cdConfig: CountdownConfig = {
    leftTime: 30,
    format: 'mm:ss',
    demand: true,
    prettyText: (text) => {
      return text
        .split(':')
        .map((v) => `<span class="item">${v}</span>`)
        .join('<span class="dots">:</span>');
    },
  };

  private _sessionId: string;

  constructor(
    private _authService: AuthService
  ) { }

  ngOnInit() {
  }

  send(form: NgForm) {
    this.error = null;
    this.spinner = true;
    let dataToSend: any;
    let req: string;

    // prepare data to send
    if (this.secondStep) {
      dataToSend = {
        sessionId: this._sessionId,
        code: form.value.secret.toString()
      };
      req = 'twoFa';
    } else {
      this._sessionId = null;

      dataToSend = {
        email: form.value.email,
        password: form.value.password
      };
      req = 'login';
    }

    // clear form on view
    form.reset();

    this._authService[req](dataToSend).subscribe(res => {
      this._sessionId = res.sessionId;

      setTimeout(() => {
        this.spinner = false;
        this.secondStep = true;

        if (req === 'twoFa') {
          this._countdown.stop();
          this.success = true;
        } else {
          setTimeout(() => this._countdown.begin(), 100);
        }
      }, 2000);

    }, error => {
      setTimeout(() => {
        if (!this.secondStep) this._sessionId = null;
        this.error = {message: (this.secondStep) ? 'Unknown error occured' : 'Unknown server error!'};
        this.spinner = false;
      }, 2000);
    });
  }

  cdEvent(event) {
    if (event.action === 'done') {
      this._sessionId = null;
      this.expired = true;
      this.error = {message: 'Session expired'};
    }
  }
}
